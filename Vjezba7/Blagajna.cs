﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vjezba7 {
    class Blagajna<Racun> {
        public Blagajna() {
            lista = new List<IRacun>();
        }
        
        void DodajRacun(IRacun racun) {
            lista.Add(racun);
        }

        private List<IRacun> lista;
    }
}