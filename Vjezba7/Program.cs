﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vjezba7 {
    class Racun<T> : IRacun {

        public Racun(T iznosRacuna) {
            IznosRacuna = iznosRacuna;
            datumIzdavanja = DateTime.UtcNow;
        }

        public decimal dohvatiIznos() {
            return Convert.ToDecimal(IznosRacuna);
        }

        public DateTime dohvatiDatumIzdavanja() {
            return datumIzdavanja;
        }

     public T IznosRacuna {
            get {
                return iznosRacuna;
            }
            set {
                if(Convert.ToDouble(value) < 10) {
                    throw new IznimkaBlagajne ("Manje od 10");
                } else {
                    iznosRacuna = value;
                    datumIzdavanja = DateTime.UtcNow;
                }
            }
        }



        private T iznosRacuna;
        private DateTime datumIzdavanja { get; set; }
    }
}
